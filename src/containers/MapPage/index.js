import React from "react";
import {
  getListOfDevices,
  getDeviceLocations,
  loadState
} from "../../utils/apiUtils";
import MyMapComponent from "../../components/Map";

class Map extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      devices: [],
      checkedDevicesLocations: {}
    };
  }

  componentDidMount() {
    setTimeout(
      () =>
        getListOfDevices(loadState().token).then(res =>
          this.setState({ devices: res.result })
        ),
      1000
    );
  }

  onInputChange = e => {
    let checkedDevicesLocations1 = this.state.checkedDevicesLocations;
    if (e.target.checked) {
      let locations = [];
      getDeviceLocations(e.target.value).then(res => {
        res.json.result.map(x => {
          if (x && x.gps) {
            let newObj = {};
            newObj["lat"] = x.gps[0];
            newObj["lng"] = x.gps[1];
            locations.push(newObj);
          }
        });
        checkedDevicesLocations1[res.device] = locations;
        this.setState({ checkedDevicesLocations: checkedDevicesLocations1 });
      });
    }
    if (!e.target.checked) {
      delete checkedDevicesLocations1[e.target.value];
      this.setState({ checkedDevicesLocations: checkedDevicesLocations1 });
    }
  };

  render() {
    return (
      <div className="container-fluid">
        <div className={"row"}>
          <div className={"col-md-8"}>
            <MyMapComponent
              isMarkerShown
              googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyDf4nIBlAk7u7z2kpZE-GkqM3W_8BM3Dk8&v=3.exp&libraries=geometry,drawing,places"
              loadingElement={<div style={{ height: `100%` }} />}
              containerElement={
                <div
                  style={{
                    height: `400px`,
                    height: `100%`,
                    position: "fixed",
                    top: "50px",
                    left: "10px",
                    bottom: "10px",
                    width: "800px",
                    overflowY: "scroll"
                  }}
                />
              }
              mapElement={<div style={{ height: `100%` }} />}
              locations={this.state.checkedDevicesLocations}
            />
          </div>
          <div className={"col-md-4"}>{this.listOfDevices()}</div>
        </div>
      </div>
    );
  }

  listOfDevices = () =>
    this.state.devices.map(x => (
      <div key={x.device}>
        <input
          key={x.device}
          onChange={this.onInputChange}
          type="checkbox"
          value={x.device}
        />
        {x.device}
      </div>
    ));
}

export default Map;
