import React, { Component } from "react";
import { Auth } from "../../utils/apiUtils";
import { Redirect } from "react-router-dom";

class Login extends Component {
  state = { redirectToReferrer: false, username: "", password: "" };

  login = e => {
    e.preventDefault();
    Auth.authenticate(this.state.username, this.state.password, () => {
      this.setState({ redirectToReferrer: true });
    });
  };

  handleInput = e => {
    this.setState({ [e.target.id]: e.target.value });
  };

  render() {
    let { from } = this.props.location.state || { from: { pathname: "/" } };
    let { redirectToReferrer } = this.state;

    if (redirectToReferrer) return <Redirect to={from} />;

    return (
      <form onSubmit={this.login}>
        <p>You must log in</p>
        <input
          onChange={this.handleInput}
          id="username"
          value={this.state.username}
          type="text"
          placeholder={"username"}
          required
        />
        <input
          onChange={this.handleInput}
          id="password"
          value={this.state.password}
          type="password"
          placeholder="password"
          required
        />
        <input type="submit" />
      </form>
    );
  }
}

export default Login;
