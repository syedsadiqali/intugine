import React from "react";
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker
} from "react-google-maps";

class MyMapComponent extends React.Component {
  constructor(props) {
    super(props);
  }

  getMarkers = () => {
    let locations1 = this.props.locations;
    let markers = [];
    for (var key in locations1) {
      if (locations1.hasOwnProperty(key)) {
        markers.push(
          <Marker
            options={{
              icon:
                "https://assetsstatic.s3.ap-south-1.amazonaws.com/navigation.svg"
            }}
            key={key}
            position={locations1[key][0]}
          />
        );
      }
    }
    return markers;
  };

  render() {
    return (
      <div>
        <GoogleMap
          defaultZoom={4}
          defaultCenter={{ lat: 20.5937, lng: 78.9629 }}
        >
          {this.getMarkers()}
        </GoogleMap>
      </div>
    );
  }
}

export default withScriptjs(withGoogleMap(MyMapComponent));
