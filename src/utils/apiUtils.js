export const isUserAuth = () => {
  let state = loadState();
  if (
    state &&
    state.token !== "" &&
    Date.now() - new Date(state.time) < 60 * 60 * 1000
  ) {
    return true;
  }
  localStorage.clear();
  return false;
};

// localStorage.js
export const loadState = () => {
  try {
    const serializedState = localStorage.getItem("state");
    if (serializedState === null) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch (err) {
    return undefined;
  }
};

// localStorage.js
export const saveState = state => {
  try {
    console.log(state);
    const serializedState = JSON.stringify(state);
    localStorage.setItem("state", serializedState);
  } catch {
    // ignore write errors
  }
};
export const Auth = {
  isAuthenticated: isUserAuth(),
  authenticate(username, password, cb) {
    fetch(
      "https://dl5opah3vc.execute-api.ap-south-1.amazonaws.com/latest/login",
      {
        method: "POST",
        headers: new Headers({
          Authorization: `Basic ${btoa(`${username}:${password}`)}`
        })
      }
    ).then(response => {
      if (!response.ok) throw new Error(response.status);
      response
        .json()
        .then(res => saveState({ token: res.token, time: Date.now() }));
      this.isAuthenticated = true;
      cb();
    });
  },
  signout(cb) {
    localStorage.clear();
    this.isAuthenticated = false;
    cb();
  }
};

export const getListOfDevices = (token) => {
  return fetch(
    "https://dl5opah3vc.execute-api.ap-south-1.amazonaws.com/latest/devices",
    { method: "GET", headers: { Authorization: "Bearer " + token } }
  )
    .then(res => res.json().then(json => json))
    .catch(err => console.log(err));
};

export const getDeviceLocations = device => {
  return fetch(
    `https://dl5opah3vc.execute-api.ap-south-1.amazonaws.com/latest?device=${device}&page=1`,
    { method: "GET", headers: { Authorization: "Bearer " + loadState().token } }
  )
    .then(res =>
      res.json().then(json => {
        return { json, device };
      })
    )
    .catch(err => console.log(err));
};
